#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
#
# If you have user-specific configurations you would like
# to apply, you may also create user-customizations.sh,
# which will be run after this script.

#!/bin/bash

echo "Setting known hosts"
truncate -s 0 ~/.ssh/known_hosts
ssh-keyscan github.com >> ~/.ssh/known_hosts


if [[ ! -d ${HOME}/.dotfiles ]]; then
  echo "Downloading .dotfiles..."
  git clone git@github.com:jayywalker/.dotfiles.git ~/.dotfiles > /dev/null 2>&1 
  bash ~/.dotfiles/install.sh > /dev/null 2>&1 
  echo ".dotfiles installed"
fi

if command -v given-command > /dev/null 2>&1; then
  echo "Yarn already installed"
else
  echo "Installing Yarn"
  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

  sudo apt-get update -y > /dev/null 2>&1
  sudo apt-get install -y --no-install-recommends yarn > /dev/null 2>&1
fi
